import { Injectable } from '@angular/core';
import { HttpService } from './common/http.service';
import { UserGroupBlank } from '../domain/usersGroups/userGroupBlank';
import { environment } from 'src/environments/environment';
import { ResponseAPI } from '../tools/responseApi';
import { map } from 'rxjs/operators';
import { UserGroupDTO } from '../dto/usersGroups/userGroupDTO';
import { RequestUserGroupDTO } from '../dto/usersGroups/requestUserGroupDTO';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { ResponseUserGroupDTO } from '../dto/usersGroups/responseUserGroupDTO';
import { UserGroup } from '../domain/usersGroups/userGroup';

@Injectable({
  providedIn: 'root'
})
export class UsersGroupsService {

  constructor(private httpService: HttpService) { }

  add(blank: UserGroupBlank) {
    const url = `${environment.RESOURCE_API_URL}/v1/usersGroups/add`;

    return this.httpService.post(url, this.buildRequestUserGroupDTO(blank)).pipe(map((response: ResponseAPI<null>) => {
      return response;
    }));
  }

  edit(blank: UserGroupBlank) {
    const url = `${environment.RESOURCE_API_URL}/v1/usersGroups/edit`;

    return this.httpService.put(url, this.buildRequestUserGroupDTO(blank)).pipe(map((response: ResponseAPI<null>) => {
      return response;
    }));
  }

  getUserGroupBlank(userGroupId: string, withUser: boolean): Observable<UserGroupBlank> {
    const url = `${environment.RESOURCE_API_URL}/v1/usersGroups/getUserGroupById`;

    const params = new HttpParams()
      .set('userGroupId', userGroupId);

    return this.httpService.get(url, params).pipe(map((response: ResponseAPI<ResponseUserGroupDTO>) => {
      let employee = null;

      if (response.success) {
        employee = new UserGroupBlank(response.data);
      }

      return employee;
    }));
  }

  getUsersGroups(): Observable<UserGroup[]> {
    const url = `${environment.RESOURCE_API_URL}/v1/usersGroups/getUsersGroups`;

    return this.httpService.get(url).pipe(map((response: ResponseAPI<ResponseUserGroupDTO[]>) => {
      const usersGroups = [];

      if (response.success) {
        for (const userGroupDTO of response.data) {
          usersGroups.push(new UserGroup(userGroupDTO));
        }
      }

      return usersGroups;
    }));
  }

  private buildRequestUserGroupDTO(blank: UserGroupBlank): UserGroupDTO {
    return new RequestUserGroupDTO(blank.id, blank.name, blank.users.map(x => x.userId));
  }
}
