import { Injectable, OnDestroy } from '@angular/core';
import { AuthenticationResponse } from '../tools/authenticationResponse';
import { Token } from '../tools/token';
import { Keys } from '../tools/keys';
import { HttpService } from './common/http.service';
import { ResponseAPI } from '../tools/responseApi';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(private httpService: HttpService) { }

  public signIn(username: string, password: string) {
    const url = `${environment.AUTH_API_URL}/auth`;

    return this.httpService.post(url, {username, password}).pipe(map((response: ResponseAPI<AuthenticationResponse>) => {
      this.saveToken(response.data);
      return response;
    }));
  }

  private saveToken(authenticationResponse: AuthenticationResponse) {
    const token = new Token(authenticationResponse.accessToken, authenticationResponse.refreshToken, '');
    localStorage.setItem(Keys.TOKEN, JSON.stringify(token));
  }
}
