import { Injectable } from '@angular/core';
import { HttpService } from './common/http.service';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ResponseAPI } from '../tools/responseApi';
import { map } from 'rxjs/operators';
import { MenuDTO } from '../dto/menu/menuDTO';
import { Menu } from '../domain/menu/menu';
import { MenuItem } from '../domain/menu/menuItem';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private httpService: HttpService) { }

  getMenu(): Observable<Menu> {
    const url = `${environment.RESOURCE_API_URL}/v1/menu/get`;

    return this.httpService.get(url).pipe(map((response: ResponseAPI<MenuDTO>) => {
      const menuItems: MenuItem[] = [];
      let menu: Menu = new Menu(menuItems);

      if (response.success) {
        if (!response.data) {
          return menu;
        }

        for (const menuItem of response.data.menuItems) {
          menuItems.push(new MenuItem(menuItem));
        }

        menu = new Menu(menuItems);
      }

      return menu;
    }));
  }
}
