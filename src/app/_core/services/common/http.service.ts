import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseAPI } from '../../tools/responseApi';

@Injectable({ providedIn: 'root' })
export class HttpService {

    constructor(private http: HttpClient) {}

    get(url: string, params: HttpParams = null) {
        if (params === null) {
            return this.http.get(url);
        } else {
            return this.http.get(url, { params });
        }
    }

    post(url: string, data: object) {
        return this.http.post(url, data);
    }

    put(url: string, data: object) {
        return this.http.put(url, data);
    }

    delete(url: string, params: HttpParams) {
        return this.http.delete(url, { params });
    }
}
