import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Keys } from '../../tools/keys';
import { SnackBarService } from '../../modules/snack_bar/snack-bar.service';
import { Router } from '@angular/router';
import { Token } from '../../tools/token';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private snackBarService: SnackBarService, private router: Router) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const tokenJson = localStorage.getItem(Keys.TOKEN);
        const token: Token = JSON.parse(tokenJson);

        const tokenString: string = token == null ? null : token.access;

        if (tokenString) {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + tokenString) });
        }

        request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });

        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
                if (error.status === 401) {
                    if (this.router.url === '/auth') { this.snackBarService.error(error.error.message); }
                    this.router.navigate(['/auth']);

                    return of(error.message);
                }

                if (error.status === 403) {
                    this.snackBarService.error(error.error.message);

                    return of(error.message);
                }

                throw error;
        }) as any);
    }
}
