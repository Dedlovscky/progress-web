import { Injectable } from '@angular/core';
import { ResponseAPI } from '../tools/responseApi';
import { HttpService } from './common/http.service';
import { EmployeeBlank } from '../domain/employees/employeeBlank';
import { environment } from 'src/environments/environment';
import { EmployeeDTO } from '../dto/employees/employeeDTO';
import { map } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../domain/employees/employee';
import { RequestEmployeeDTO } from '../dto/employees/requestEmployeeDTO';
import { RequestUserDTO } from '../dto/users/requestUserDTO';
import { ResponseEmployeeDTO } from '../dto/employees/responseEmployeeDTO';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  constructor(private httpService: HttpService) { }

  add(blank: EmployeeBlank) {
    const url = `${environment.RESOURCE_API_URL}/v1/employees/add`;

    return this.httpService.post(url, this.buildRequestEmployeeDTO(blank)).pipe(map((response: ResponseAPI<null>) => {
      return response;
    }));
  }

  edit(blank: EmployeeBlank) {
    const url = `${environment.RESOURCE_API_URL}/v1/employees/edit`;

    return this.httpService.put(url, this.buildRequestEmployeeDTO(blank)).pipe(map((response: ResponseAPI<null>) => {
      return response;
    }));
  }

  remove(employeeId: string) {
    const url = `${environment.RESOURCE_API_URL}/v1/employees/remove`;

    const params = new HttpParams()
      .set('employeeId', employeeId);

    return this.httpService.delete(url, params).pipe(map((response: ResponseAPI<null>) => {
      return response;
    }));
  }

  getEmployeeBlank(employeeId: string, withUser: boolean): Observable<EmployeeBlank> {
    const url = `${environment.RESOURCE_API_URL}/v1/employees/getEmployeeById`;

    const params = new HttpParams()
      .set('employeeId', employeeId)
      .set('withUser', `${withUser}`);

    return this.httpService.get(url, params).pipe(map((response: ResponseAPI<ResponseEmployeeDTO>) => {
      let employee = null;

      if (response.success) {
        employee = new EmployeeBlank(response.data);
      }

      return employee;
    }));
  }

  getEmployees(): Observable<Employee[]> {
    const url = `${environment.RESOURCE_API_URL}/v1/employees/getEmployees`;

    return this.httpService.get(url).pipe(map((response: ResponseAPI<ResponseEmployeeDTO[]>) => {
      const employees = [];

      if (response.success) {
        for (const employeeDTO of response.data) {
          employees.push(new Employee(employeeDTO));
        }
      }

      return employees;
    }));
  }

  private buildRequestEmployeeDTO(blank: EmployeeBlank): EmployeeDTO {
    const user = blank.userBlank === null
      ? null
      : new RequestUserDTO(blank.userBlank.id, blank.userBlank.username, blank.userBlank.password, blank.userBlank.role);

    return new RequestEmployeeDTO(blank.id, blank.lastName, blank.firstName, blank.patronymic, blank.gender, user);
  }
}
