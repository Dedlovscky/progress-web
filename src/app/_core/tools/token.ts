export class Token {
    access: string;
    refresh: string;
    expirationTime: any;

    constructor(access: string, refresh: string, expirationTime: any) {
        this.access = access;
        this.refresh = refresh;
        this.expirationTime = expirationTime;
    }
}