export class ResponseAPI<T> {
    success: boolean;
    message: string;
    data: T | null;
}
