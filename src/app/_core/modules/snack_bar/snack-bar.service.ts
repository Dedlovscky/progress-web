import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { SnackBar } from './snack-bar.model';

@Injectable({ providedIn: 'root' })
export class SnackBarService {
    private subject = new Subject<SnackBar>();

    private init(snackBar: SnackBar) {
        this.subject.next(snackBar);
    }

    success(message: string, action?: string) {
        this.init(new SnackBar({ message, action, panelClass: 'success'}));
    }

    error(message: string, action?: string) {
        this.init(new SnackBar({ message, action, panelClass: 'error' }));
    }

    info(message: string, action?: string) {
        this.init(new SnackBar({ message, action, panelClass: 'info' }));
    }

    warning(message: string, action?: string) {
        this.init(new SnackBar({ message, action, panelClass: 'warning' }));
    }

    get(): Observable<SnackBar> {
        return this.subject.asObservable();
    }

    clear() {
        this.subject.next();
    }
}