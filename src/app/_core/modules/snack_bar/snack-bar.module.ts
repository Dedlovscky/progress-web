import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SnackBarComponent } from './snack-bar.component';

@NgModule({
    imports: [CommonModule],
    declarations: [SnackBarComponent],
    exports: [SnackBarComponent]
})
export class SnackBarModule { }