export class SnackBar {
    message: string;
    action: string;
    panelClass: string;

    constructor(init: Partial<SnackBar>) {
        Object.assign(this, init);
    }
}