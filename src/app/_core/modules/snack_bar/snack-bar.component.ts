import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SnackBarService } from './snack-bar.service';
import { SnackBar } from './snack-bar.model';

@Component({
  selector: 'app-snack-bar',
  templateUrl: './snack-bar.component.html',
  styleUrls: ['./snack-bar.component.scss']
})
export class SnackBarComponent implements OnInit {

  subscription: Subscription;
  durationInSeconds = 5;

  constructor(private snackBarService: SnackBarService) {}

  private openSnackBar(snackBar: SnackBar) {

  }

  ngOnInit() {
    this.subscription = this.snackBarService.get().subscribe((snackBar: SnackBar) => {
      this.openSnackBar(snackBar);
    });
  }

}
