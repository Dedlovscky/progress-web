import { Role } from '../users/types/role';
import { GroupUserDTO } from '../../dto/groupUsers/groupUserDTO';

export class GroupUser {
    userId: string;
    userRole: Role;
    employeeFirstName: string;
    employeeLastName: string;
    employeePatronymic: string;

    constructor(groupUser: GroupUserDTO) {
        this.userId = groupUser.userId;
        this.userRole = groupUser.userRole;
        this.employeeFirstName = groupUser.employeeFirstName;
        this.employeeLastName = groupUser.employeeLastName;
        this.employeePatronymic = groupUser.employeePatronymic;
    }
}
