import { ResponseUserGroupDTO } from '../../dto/usersGroups/responseUserGroupDTO';
import { GroupUser } from '../groupUsers/groupUser';

export class UserGroup {
    id: string;
    name: string;
    users: GroupUser[];

    constructor(userGroup: ResponseUserGroupDTO) {
        this.id = userGroup.id;
        this.name = userGroup.name;
        this.users = userGroup.users.map(x => new GroupUser(x));
    }
}
