import { ResponseUserGroupDTO } from '../../dto/usersGroups/responseUserGroupDTO';
import { ResponseUserDTO } from '../../dto/users/responseUserDTO';
import { GroupUserDTO } from '../../dto/groupUsers/groupUserDTO';

export class UserGroupBlank {
    id: string | null;
    name: string | null;
    users: GroupUserDTO[];

    constructor(userGroup?: ResponseUserGroupDTO) {
        if (userGroup) {
            this.id = userGroup.id;
            this.name = userGroup.name;
            this.users = userGroup.users;
        } else {
            this.id = null;
            this.name = null;
            this.users = [];
        }
    }
}
