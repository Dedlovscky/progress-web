export enum Role {
    NONE = 255,
    ARCHITECT = 254,
    ADMINISTRATOR = 0,
    USER = 1
}
