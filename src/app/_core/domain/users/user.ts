import { Role } from './types/role';
import { ResponseUserDTO } from '../../dto/users/responseUserDTO';

export class User {
    id: string;
    username: string;
    role: Role;

    constructor(user: ResponseUserDTO) {
        this.id = user.id;
        this.username = user.username;
        this.role = user.role;
    }
}
