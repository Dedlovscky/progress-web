import { Role } from './types/role';
import { ResponseUserDTO } from '../../dto/users/responseUserDTO';

export class UserBlank {
    id: string | null;
    username: string | null;
    password: string | null;
    role: Role | null;

    constructor(user?: ResponseUserDTO) {
        if (!user) {
            this.id = null;
            this.username = null;
            this.password = null;
            this.role = Role.NONE;
        } else {
            this.id = user.id;
            this.username = user.username;
            this.password = null;
            this.role = user.role;
        }
    }
}
