import { MenuItem } from './menuItem';

export class Menu {
    menuItems: MenuItem[];

    constructor(menuItems: MenuItem[]) {
        this.menuItems = menuItems;
    }
}
