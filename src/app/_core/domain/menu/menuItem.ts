import { MenuItemDTO } from '../../dto/menu/menuItemDTO';

export class MenuItem {
    id: number;
    icon: string;
    title: string;
    cssClass: string;
    url: string;

    constructor(menuItemDTO: MenuItemDTO) {
        this.id = menuItemDTO.id;
        this.icon = menuItemDTO.icon;
        this.title = menuItemDTO.title;
        this.cssClass = menuItemDTO.cssClass;
        this.url = menuItemDTO.url;
    }
}