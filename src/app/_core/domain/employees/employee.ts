import { Gender } from './types/gender';
import { ResponseEmployeeDTO } from '../../dto/employees/responseEmployeeDTO';
import { User } from '../users/user';

export class Employee {
    id: string;
    lastName: string;
    firstName: string;
    patronymic: string;
    gender: Gender;
    user: User | null;

    constructor(employeeDTO: ResponseEmployeeDTO) {
        this.id = employeeDTO.employeeId;
        this.lastName = employeeDTO.lastName;
        this.firstName = employeeDTO.firstName;
        this.patronymic = employeeDTO.patronymic;
        this.gender = employeeDTO.gender;
        this.user = employeeDTO.user === null ? null : new User(employeeDTO.user);
    }
}
