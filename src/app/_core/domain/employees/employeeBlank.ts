import { Gender } from './types/gender';
import { UserBlank } from '../users/userBlank';
import { ResponseEmployeeDTO } from '../../dto/employees/responseEmployeeDTO';

export class EmployeeBlank {
    id: string | null;
    lastName: string | null;
    firstName: string | null;
    patronymic: string | null;
    gender: Gender;
    userBlank: UserBlank | null;

    constructor(employee?: ResponseEmployeeDTO) {
        if (!employee) {
            this.id = null;
            this.lastName = null;
            this.firstName = null;
            this.patronymic = null;
            this.gender = Gender.NONE;
            this.userBlank = new UserBlank();
        } else {
            this.id = employee.employeeId;
            this.lastName = employee.lastName;
            this.firstName = employee.firstName;
            this.patronymic = employee.patronymic;
            this.gender = employee.gender;
            this.userBlank = new UserBlank(employee.user);
        }
    }
}
