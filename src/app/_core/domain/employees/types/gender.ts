export enum Gender {
    NONE = 255,
    MALE = 0,
    FEMALE = 1
}
