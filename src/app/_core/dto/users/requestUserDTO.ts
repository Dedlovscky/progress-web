import { Role } from '../../domain/users/types/role';
import { UserDTO } from './userDTO';

export class RequestUserDTO extends UserDTO {
    userId: string;
    username: string;
    password: string;
    role: Role;

    constructor(userId: string, username: string, password: string, role: Role) {
        super(userId, username, role);
        this.password = password;
    }
}
