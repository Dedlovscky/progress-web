import { Role } from '../../domain/users/types/role';
import { UserDTO } from './userDTO';

export class ResponseUserDTO extends UserDTO {
    constructor(id: string, username: string, role: Role) {
        super(id, username, role);
    }
}
