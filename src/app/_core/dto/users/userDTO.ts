import { Role } from '../../domain/users/types/role';

export abstract class UserDTO {
    id: string;
    username: string;
    role: Role;

    constructor(id: string, username: string, role: Role) {
        this.id = id;
        this.username = username;
        this.role = role;
    }
}
