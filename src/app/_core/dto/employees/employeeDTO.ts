import { Gender } from '../../domain/employees/types/gender';

export abstract class EmployeeDTO {
    employeeId: string;
    lastName: string;
    firstName: string;
    patronymic: string;
    gender: Gender;

    constructor(employeeId: string, lastName: string, firstName: string, patronymic: string, gender: Gender) {
        this.employeeId = employeeId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.gender =  gender;
    }
}
