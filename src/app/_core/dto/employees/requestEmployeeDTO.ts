import { EmployeeDTO } from './employeeDTO';
import { Gender } from '../../domain/employees/types/gender';
import { RequestUserDTO } from '../users/requestUserDTO';

export class RequestEmployeeDTO extends EmployeeDTO {
    user?: RequestUserDTO;

    constructor(employeeId: string, lastName: string, firstName: string, patronymic: string, gender: Gender, user?: RequestUserDTO) {
        super(employeeId, lastName, firstName, patronymic, gender);
        this.user = user;
    }
}
