import { ResponseUserDTO } from '../users/responseUserDTO';
import { Gender } from '../../domain/employees/types/gender';
import { EmployeeDTO } from './employeeDTO';

export class ResponseEmployeeDTO extends EmployeeDTO {
    user?: ResponseUserDTO;

    constructor(employeeId: string, lastName: string, firstName: string, patronymic: string, gender: Gender, user?: ResponseUserDTO) {
        super(employeeId, lastName, firstName, patronymic, gender);
        this.user = user;
    }
}
