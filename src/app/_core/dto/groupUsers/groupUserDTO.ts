import { Role } from '../../domain/users/types/role';

export class GroupUserDTO {
    readonly userId: string;
    readonly userRole: Role;
    readonly employeeFirstName: string;
    readonly employeeLastName: string;
    readonly employeePatronymic: string;

    constructor(userId: string, userRole: Role, employeeFirstName: string, employeeLastName: string, employeePatronymic: string) {
        this.userId = userId;
        this.userRole = userRole;
        this.employeeFirstName = employeeFirstName;
        this.employeeLastName = employeeLastName;
        this.employeePatronymic = employeePatronymic;
    }
}
