import { UserGroupDTO } from './userGroupDTO';
import { GroupUserDTO } from '../groupUsers/groupUserDTO';

export class ResponseUserGroupDTO extends UserGroupDTO {
    users: GroupUserDTO[];

    constructor(id: string, name: string, users: GroupUserDTO[]) {
        super(id, name);
        this.users = users;
    }
}
