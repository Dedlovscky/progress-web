import { UserGroupDTO } from './userGroupDTO';

export class RequestUserGroupDTO extends UserGroupDTO {
    userIds: string[];

    constructor(userGroupId: string, userGroupName: string, userIds: string[]) {
        super(userGroupId, userGroupName);
        this.userIds = userIds;
    }
}
