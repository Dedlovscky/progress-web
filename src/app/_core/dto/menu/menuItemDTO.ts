export class MenuItemDTO {
    id: number;
    icon: string;
    title: string;
    cssClass: string;
    url: string;

    constructor(id: number, icon: string, title: string, cssClass: string, url: string) {
        this.id = id;
        this.icon = icon;
        this.title = title;
        this.cssClass = cssClass;
        this.url = url;
    }
}
