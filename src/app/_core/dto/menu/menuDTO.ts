import { MenuItemDTO } from './menuItemDTO';

export class MenuDTO {
    menuItems: MenuItemDTO[];

    constructor(menuItems: MenuItemDTO[]) {
        this.menuItems = menuItems;
    }
}
