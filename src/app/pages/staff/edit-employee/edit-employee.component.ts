import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeesService } from 'src/app/_core/services/employees.service';
import { EmployeeBlank } from 'src/app/_core/domain/employees/employeeBlank';
import { Role } from 'src/app/_core/domain/users/types/role';
import { Gender } from 'src/app/_core/domain/employees/types/gender';
import { ResponseAPI } from 'src/app/_core/tools/responseApi';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss']
})
export class EditEmployeeComponent implements OnInit {

  employeeBlank: EmployeeBlank;
  isUser = false;
  showIsUser = false;
  confirmPassword: string;

  roles = [
    {key: Role.ADMINISTRATOR, value: 'Администратор'},
    {key: Role.USER, value: 'Пользователь'},
    {key: Role.NONE, value: 'Выберите роль в системе'}
  ];

  genders = [
    {key: Gender.MALE, value: 'Мужской'},
    {key: Gender.FEMALE, value: 'Женский'},
    {key: Gender.NONE, value: 'Укажите пол'},
  ];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private employeesService: EmployeesService
  ) {
    this.employeeBlank = new EmployeeBlank();
  }

  edit() {
    if (this.employeeBlank.userBlank.password !== this.confirmPassword) {
      console.log('Пароли не совпадают');
      return;
    }

    if (!this.isUser) {
      this.employeeBlank.userBlank = null;
    }

    this.employeesService.edit(this.employeeBlank).subscribe((response: ResponseAPI<null>) => {
      if (response.success) {
        this.router.navigateByUrl('/main/staff');
      } else {
        console.log('Ошибка сохранения сотрудника');
        // this.snackBarService.error(response.message);
      }
    });
  }

  remove() {
    const employeeId = this.activatedRoute.snapshot.paramMap.get('employeeId');

    this.employeesService.remove(employeeId).subscribe((response: ResponseAPI<null>) => {
      if (response.success) {
        this.router.navigateByUrl('/main/staff');
      } else {
        console.log('Ошибка удаления сотрудника');
        // this.snackBarService.error(response.message);
      }
    });
  }

  cancel() {
    this.router.navigateByUrl('/main/staff');
  }

  ngOnInit() {
    const employeeId = this.activatedRoute.snapshot.paramMap.get('employeeId');

    this.employeesService.getEmployeeBlank(employeeId, true).subscribe((employeeBlank: EmployeeBlank) => {
      this.employeeBlank = employeeBlank;
      this.isUser = employeeBlank.userBlank.id !== null;
      this.showIsUser = employeeBlank.userBlank.id === null;
    });
  }
}
