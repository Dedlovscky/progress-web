import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeBlank } from 'src/app/_core/domain/employees/employeeBlank';
import { EmployeesService } from 'src/app/_core/services/employees.service';
import { ResponseAPI } from 'src/app/_core/tools/responseApi';
import { Role } from 'src/app/_core/domain/users/types/role';
import { Gender } from 'src/app/_core/domain/employees/types/gender';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {

  employeeBlank: EmployeeBlank;
  isUser = false;
  confirmPassword: string;

  roles = [
    {key: Role.ADMINISTRATOR, value: 'Администратор'},
    {key: Role.USER, value: 'Пользователь'},
    {key: Role.NONE, value: 'Выберите роль в системе'}
  ];

  genders = [
    {key: Gender.MALE, value: 'Мужской'},
    {key: Gender.FEMALE, value: 'Женский'},
    {key: Gender.NONE, value: 'Укажите пол'},
  ];


  constructor(private router: Router, private employeesService: EmployeesService) {
    this.employeeBlank = new EmployeeBlank();
  }

  add() {
    if (!this.isUser) {
      this.employeeBlank.userBlank = null;
    } else {
      if (this.employeeBlank.userBlank.password !== this.confirmPassword) {
        console.log('Пароли не совпадают');
        return;
      }
    }

    this.employeesService.add(this.employeeBlank).subscribe((response: ResponseAPI<null>) => {
      if (response.success) {
        this.router.navigateByUrl('/main/staff');
      } else {
        console.log(response.message);
        // this.snackBarService.error(response.message);
      }
    });
  }

  cancel() {
    this.router.navigateByUrl('/main/staff');
  }

  ngOnInit() {
  }
}
