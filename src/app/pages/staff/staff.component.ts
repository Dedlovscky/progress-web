import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeesService } from 'src/app/_core/services/employees.service';
import { SnackBarService } from 'src/app/_core/modules/snack_bar/snack-bar.service';
import { Employee } from 'src/app/_core/domain/employees/employee';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit {

  employees: Employee[];

  constructor(
    private router: Router,
    private snackBarService: SnackBarService,
    private employeesService: EmployeesService
  ) { }

  search() {

  }

  addEmployee() {
    this.router.navigateByUrl('/main/staff/add');
  }

  editEmployee(employee: Employee) {
    this.router.navigateByUrl(`/main/staff/edit/${employee.id}`);
  }

  ngOnInit() {
    this.employeesService.getEmployees().subscribe((employees: Employee[]) => {
        this.employees = employees;
    });
  }
}
