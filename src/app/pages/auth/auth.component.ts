import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SnackBarService } from '../../_core/modules/snack_bar/snack-bar.service';
import { ResponseAPI } from '../../_core/tools/responseApi';
import { AuthenticationResponse } from '../../_core/tools/authenticationResponse';
import { AuthService } from '../../_core/services/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})

export class AuthComponent implements OnInit {

  hide = true;
  username = '';
  password = '';
  maxUsernameLength = 48;
  maxPasswordLength = 36;

  constructor(private router: Router, private snackBarService: SnackBarService, private authService: AuthService) {}

  signIn() {
    // this.router.navigateByUrl('/main/staff');
    this.authService.signIn(this.username, this.password).subscribe((response: ResponseAPI<AuthenticationResponse>) => {
      if (response.success) {
        this.router.navigateByUrl('/main/desktop');
      } else {
        this.snackBarService.error(response.message);
      }
    });
  }

  getUsernameLength() {
     return `${this.username.length || 0} / ${this.maxUsernameLength}`;
  }

  getPasswordLength() {
    return `${this.password.length || 0} / ${this.maxPasswordLength}`;
  }

  ngOnInit() {
  }
}
