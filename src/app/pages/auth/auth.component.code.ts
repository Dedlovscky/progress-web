import { Router } from '@angular/router';
import { ResponseAPI } from '../../_core/tools/responseApi';
import { Keys } from '../../_core/tools/keys';
import { Token } from '../../_core/tools/token';
import { AuthenticationResponse } from '../../_core/tools/authenticationResponse';

export class AuthCode {

  private token: Token;

  constructor(private router: Router) {
  }

  login(response: ResponseAPI<AuthenticationResponse>) {
      if (response.success) {
        this.saveToken(response.data);
        this.router.navigateByUrl('/main');
      } else {
        this.router.navigateByUrl('/auth');
      }
  }

  private saveToken(authenticationResponse: AuthenticationResponse) {
    this.token = new Token(authenticationResponse.accessToken, authenticationResponse.refreshToken, '');
    localStorage.setItem(Keys.TOKEN, JSON.stringify(this.token));
  }
}
