import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersGroupsService } from 'src/app/_core/services/users-groups.service';
import { UserGroupBlank } from 'src/app/_core/domain/usersGroups/userGroupBlank';
import { ResponseAPI } from 'src/app/_core/tools/responseApi';

@Component({
  selector: 'app-edit-user-group',
  templateUrl: './edit-user-group.component.html',
  styleUrls: ['./edit-user-group.component.scss']
})
export class EditUserGroupComponent implements OnInit {

  userGroupBlank: UserGroupBlank;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private usersGroupsService: UsersGroupsService
  ) { }

  edit() {
    this.usersGroupsService.edit(this.userGroupBlank).subscribe((response: ResponseAPI<null>) => {
      if (response.success) {
        this.router.navigateByUrl('/main/users-groups');
      } else {
        console.log(response.message);
        // this.snackBarService.error(response.message);
      }
    });
  }

  cancel() {
    this.router.navigateByUrl('/main/users-groups');
  }

  ngOnInit() {
    const userGroupId = this.activatedRoute.snapshot.paramMap.get('userGroupId');

    this.usersGroupsService.getUserGroupBlank(userGroupId, true).subscribe((userGroupBlank: UserGroupBlank) => {
      this.userGroupBlank = userGroupBlank;
    });
  }
}
