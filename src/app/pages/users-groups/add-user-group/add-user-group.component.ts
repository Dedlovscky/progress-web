import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersGroupsService } from 'src/app/_core/services/users-groups.service';
import { UserGroupBlank } from 'src/app/_core/domain/usersGroups/userGroupBlank';
import { ResponseAPI } from 'src/app/_core/tools/responseApi';

@Component({
  selector: 'app-add-user-group',
  templateUrl: './add-user-group.component.html',
  styleUrls: ['./add-user-group.component.scss']
})
export class AddUserGroupComponent implements OnInit {

  userGroupBlank: UserGroupBlank = new UserGroupBlank();

  constructor(private router: Router, private usersGroupsService: UsersGroupsService) { }

  add() {
    this.usersGroupsService.add(this.userGroupBlank).subscribe((response: ResponseAPI<null>) => {
      if (response.success) {
        this.router.navigateByUrl('/main/users-groups');
      } else {
        console.log(response.message);
        // this.snackBarService.error(response.message);
      }
    });
  }

  cancel() {
    this.router.navigateByUrl('/main/users-groups');
  }

  ngOnInit() {
  }

}
