import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserGroup } from 'src/app/_core/domain/usersGroups/userGroup';
import { UsersGroupsService } from 'src/app/_core/services/users-groups.service';

@Component({
  selector: 'app-users-groups',
  templateUrl: './users-groups.component.html',
  styleUrls: ['./users-groups.component.scss']
})
export class UsersGroupsComponent implements OnInit {

  usersGroups: UserGroup[] = [];

  constructor(private router: Router, private usersGroupsService: UsersGroupsService) { }

  search() {

  }

  addUserGroup() {
    this.router.navigateByUrl('/main/users-groups/add');
  }

  editUserGroup(userGroup: UserGroup) {
    this.router.navigateByUrl(`/main/users-groups/edit/${userGroup.id}`);
  }

  ngOnInit() {
    this.usersGroupsService.getUsersGroups().subscribe((usersGroups: UserGroup[]) => {
      this.usersGroups = usersGroups;
    });
  }

}
