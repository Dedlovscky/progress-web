import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { ErrorInterceptor } from './_core/services/common/error.interceptor';
import { SnackBarModule } from './_core/modules/snack_bar/snack-bar.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { HttpService } from './_core/services/common/http.service';
import { SnackBarService } from './_core/modules/snack_bar/snack-bar.service';

import { AuthComponent } from './pages/auth/auth.component';
import { MainComponent } from './pages/main/main.component';
import { TopMenuComponent } from './top-menu/top-menu.component';
import { StaffComponent } from './pages/staff/staff.component';
import { AffairsComponent } from './pages/affairs/affairs.component';
import { AddEmployeeComponent } from './pages/staff/add-employee/add-employee.component';
import { EditEmployeeComponent } from './pages/staff/edit-employee/edit-employee.component';
import { DesktopComponent } from './pages/desktop/desktop.component';
import { UsersGroupsComponent } from './pages/users-groups/users-groups.component';
import { AddUserGroupComponent } from './pages/users-groups/add-user-group/add-user-group.component';
import { EditUserGroupComponent } from './pages/users-groups/edit-user-group/edit-user-group.component';

const itemRoutes: Routes = [
  { path: 'desktop', component: DesktopComponent},
  { path: 'staff', component: StaffComponent},
  { path: 'staff/add', component: AddEmployeeComponent},
  { path: 'staff/edit/:employeeId', component: EditEmployeeComponent},
  { path: 'affairs', component: AffairsComponent },
  { path: 'users-groups', component: UsersGroupsComponent },
  { path: 'users-groups/add', component: AddUserGroupComponent },
  { path: 'users-groups/edit/:userGroupId', component: EditUserGroupComponent},
];

const appRoutes: Routes = [
  { path: '', redirectTo: '/auth', pathMatch: 'full' },
  { path: 'main', redirectTo: '/main/desktop', pathMatch: 'full' },
  { path: 'auth', component: AuthComponent },
  { path: 'main', component: MainComponent, children: itemRoutes }
];

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    MainComponent,
    TopMenuComponent,
    StaffComponent,
    AffairsComponent,
    AddEmployeeComponent,
    EditEmployeeComponent,
    DesktopComponent,
    UsersGroupsComponent,
    AddUserGroupComponent,
    EditUserGroupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SnackBarModule,
    DragDropModule
  ],
  providers: [
    HttpService,
    SnackBarService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
