import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { Router } from '@angular/router';
import { MenuService } from '../_core/services/menu.service';
import { Menu } from '../_core/domain/menu/menu';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {
  menu: Menu;

  constructor(
    private router: Router,
    private menuService: MenuService
  ) {
    this.menu = new Menu([]);
   }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.menu.menuItems, event.previousIndex, event.currentIndex);
  }

  selectedMenuItem(menuItem) {
    menuItem.cssClass = 'active-item';

    const menuItems = this.menu.menuItems.filter(x => x.id !== menuItem.id);

    for (const item of menuItems) {
      item.cssClass = '';
    }

    this.router.navigateByUrl(menuItem.url);
  }

  ngOnInit() {
    this.menuService.getMenu().subscribe((menu: Menu) => {
      this.menu = menu;
    });
  }

}
